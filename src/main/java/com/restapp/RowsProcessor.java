package com.restapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by tadasyan
 */

@Service
public class RowsProcessor {

    @Autowired
    private RowsRepository rowsRepository;

    private final Logger LOGGER = LoggerFactory.getLogger("logger");

    @Async("addFileExecutor")
    public void addFile(MultipartFile file){
        if (!file.isEmpty()){
            LOGGER.info("processing file " + file.getOriginalFilename());
            try {
                try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getInputStream()))){
                    bufferedReader.lines().forEach(line -> rowsRepository.addRow(line));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Map<String, Long> getResponse(){
        LOGGER.info("execute getResponse method");

        List<String> rows = rowsRepository.getRows();
        return rows.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }
}
