package com.restapp;


import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tadasyan
 */

@Service
public class RowsRepository {

    private static class InstanceHolder{
        public static RowsRepository instance = new RowsRepository();
    }

    private List<String> rows;

    private RowsRepository(){
        this.rows = new ArrayList<>();
    }

    public List<String> getRows(){
        return this.rows;
    }

    public void addRow(String row){
        this.rows.add(row);
    }

    @Bean(name = "rowsRepository")
    public static RowsRepository getInstance(){
        return InstanceHolder.instance;
    }
}
