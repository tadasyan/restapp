package com.restapp.controller;

import com.restapp.ResultObj;
import com.restapp.RowsProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by tadasyan
 */

@RestController
@RequestMapping("/rest/result")
public class ResultController {

    @Autowired
    private RowsProcessor rowsProcessor;

    @RequestMapping(method = RequestMethod.GET)
    public List<ResultObj> getResult(){
        List<ResultObj> result = new ArrayList<>();
        rowsProcessor.getResponse().forEach((k, v) -> result.add(new ResultObj(k, v)));
        //return result;
        return result.stream().sorted(((o1, o2) -> o2.getCount().compareTo(o1.getCount()))).collect(Collectors.toList());
    }

}
