package com.restapp.controller;

import com.restapp.RowsProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.annotation.MultipartConfig;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 * Created by tadasyan.
 */

@RestController
@RequestMapping(value = "/rest/upload")
@MultipartConfig(fileSizeThreshold = 20971520)
public class UploadController {

    private final Logger LOGGER = LoggerFactory.getLogger("logger");
    public static final String ROOT = "upload";

    private final ResourceLoader resourceLoader;
    private RowsProcessor rowsProcessor;

    @Autowired
    public UploadController(ResourceLoader resourceLoader, RowsProcessor rowsProcessor) {
        this.resourceLoader = resourceLoader;
        this.rowsProcessor = rowsProcessor;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String uploadFile(@RequestParam(name = "file") MultipartFile[] files){
        if (files.length == 0){
            return "no files";
        }
        for (int i = 0; i < files.length; i++) {
            //LOGGER.info(files[i].getOriginalFilename() + " uploaded");
            rowsProcessor.addFile(files[i]);
        }
        return "success";
    }
}
