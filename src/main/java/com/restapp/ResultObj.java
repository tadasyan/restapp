package com.restapp;

import java.util.List;

/**
 * Created by tadasyan
 */
public class ResultObj {

    private String value;
    private Long count;

    public ResultObj(String value, Long count) {
        this.value = value;
        this.count = count;
    }

    public String getValue() {
        return value;
    }

    public Long getCount() {
        return count;
    }
}
