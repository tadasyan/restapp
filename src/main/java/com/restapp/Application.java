package com.restapp;

import com.restapp.controller.UploadController;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by tadasyan
 */

@SpringBootApplication
@EnableAutoConfiguration
@EnableAsync
@Configuration
public class Application {

    @Bean
    public ThreadPoolTaskExecutor addFileExecutor(){
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        pool.setCorePoolSize(3);
        pool.setMaxPoolSize(3);
        pool.setWaitForTasksToCompleteOnShutdown(true);
        pool.initialize();
        return pool;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    /*
    @Bean
    CommandLineRunner init(){
        return (args) ->    {
            FileSystemUtils.deleteRecursively(new File(UploadController.ROOT));
            Files.createDirectory(Paths.get(UploadController.ROOT));
        };
    }
    */
}
